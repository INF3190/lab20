var express = require('express');
var router = express.Router();
var fs = require('fs');

/* GET home page. */
router.get('/', function(req, res, next) {
  //res.render('index', { title: 'Express' });
	res.redirect('/lab20.html');
});
// /uqam
router.get('/uqam', function(req, res, next) {
		res.redirect('https://www.uqam.ca');
	});

//lecture fichier json

router.get('/univ', function(req, res, next) {
	//res.redirect('https://www.uqam.ca');
	var fichier=__dirname+"/../public/javascripts/universites.json";
	var univ="";
	fs.readFile(fichier, 'utf8' , (err, data) => {
		  if (err) {
		    console.error(err)
		    return
		  }
		  univ = JSON.parse(data);
		  console.log(univ);
		  var liste = univ.liste;
		  var dl="<dl>";
		  for (var un of liste){
			  dl=dl+"<dt style='color:blue;'>"+un.sigle+"</dt>";
			  dl=dl+"<dd>"+un.nom+"</dd>";
		  }
			  dl=dl+"</dl>";
		res.send(dl);
		  //res.json(univ);
		})
		var rep=__dirname;
	 //res.json(univ);
	//res.send("Lecture fichier "+rep);
});
module.exports = router;
